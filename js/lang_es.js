var resources = {
	"menu_about":"Acerca",
	"menu_products":"Productos",
	"menu_careers":"Empleos",
	"menu_contact":"Contacto",
	"kantoo_quote": "KANTOO.COM APRENDA NOVOS IDIOMAS",
	"hero3_text1": "ES MÁS<br>ABUNDANTE<br> EN <br> <b>INGLÉS...</b>",
	"hero4_text1": "ES MÁS<br>ROMÁNTICO<br> EN <br> <b>FRANCÉS...</b>",
	"hero5_text1": "ES MÁS<br>SABROSO<br> EN <br> <b>ITALIANO...</b>",
	"hero2_text1": "ES MÁS<br>ATRACTIVO<br> EN <br> <b>PORTUGUÉS...</b>",
	"hero1_text1": "ES MÁS<br>DIVERTIDO<br> EN <br> <b>YIDDISH...</b>",
	"statistics_products":"Productos diferentes",
	"statistics_subscribers":"Millones de usuarios",
	"statistics_languages":"Idiomas",
	"title_aboutus":"QUIÉNES SOMOS",
	"about_paragraph1":"La-Mark fue fundada en 2007 con la visión de brindar oportunidades de aprendizaje de idiomas al mayor número de personas. Es por eso que nuestros productos son accesibles a todos a través de un teléfono móvil, con el mejor costo-beneficio y contenido atractivo.",
	"about_paragraph2":"Estamos orgullosos de promover el aprendizaje de idiomas alrededor del mundo y creemos que quien se mantiene comprometido con nuestros productos estará siempre aprendiendo - y esa es nuestra misión.",
	"about_paragraph3":"Más de 50 millones de usuarios disfrutan de nuestros productos, los cuales presentan: una narrativa atractiva, una integración perfecta entre la práctica de habilidades lingüísticas y tecnología, y un contenido exclusivo para todo tipo de mercado y edad.",
	"title_partners":"CON QUIÉN TRABAJAMOS",
	"title_products":"NUESTROS PRODUTOS",
	"product_slide1_title1":"Hollywood English",
	"product_slide1_text":"¡Aprende inglés con <br>las estrellas!",
	"product_slide1_title2":"HOLLYWOOD ENGLISH",
	"product_slide1_description_paragraph1":"Hollywood English ofrece una manera única e innovadora de aprender inglés. Este producto audiovisual cuenta con una presentadora que te mostrará el cotidiano de las estrellas de Hollywood - ¡su estilo de vida, los romances, las tragedias y escándalos, historias detrás del escenario, biografías, curiosidades sorprendentes y mucho más!",
	"product_slide1_description_paragraph2":"Cada video/lección presenta puntos gramaticales y vocabulario que están pedagógicamente relacionados al tema de la clase. Las lecciones en vídeo vienen acompañadas de ejercicios interactivos especialmente diseñados para consolidar las habilidades lingüísticas y para que puedas disfrutar de las historias de las estrellas más importantes de Hollywood mientras aprendes inglés.",
	"product_slide1_description_paragraph3":"¡También podrás acceder al área VIP donde encontrarás contenido extra y quizzes divertidos!",
	"product_slide1_description_paragraph4":"Con Hollywood English, ¡tu inglés está a punto de alcanzar la fama! Entonces prepárate: ¡luz, cámara, acción!",
	"product_slide2_title1":"Violetta - Mi mundo en inglés",
	"product_slide2_text":"¡Aprende inglés <br>con Violetta!",
	"product_slide2_title2":"VIOLETTA - MI MUNDO EN INGLÉS",
	"product_slide2_description_paragraph1":"Violetta - Mi mundo en inglés es un producto original de la amada Violetta de Disney, repleto de animaciones y diseñado especialmente para niños.",
	"product_slide2_description_paragraph2":"La trama lleva a los niños a un paseo con Violetta y los otros personajes originales de la serie, a través de sus emocionantes aventuras mientras aprenden habilidades fundamentales en inglés.",
	"product_slide2_description_paragraph3":"El producto ofrece una clara estructura del inglés básico al avanzado, combinando puntos gramaticales y vocabulario en una trama original.",
	"product_slide2_description_paragraph4":"¡En Violetta - Mi mundo en inglés también puedes acceder a la tienda exclusiva de Violetta con elementos especiales para coleccionistas!",
	"product_slide2_description_paragraph5":"¡Violetta y sus amigos te están esperando!",
	"product_slide3_title1":"Violetta - inglés",
	"product_slide3_text":"¡Ven a jugar con Violetta!",
	"product_slide3_title2":"VIOLETTA - INGLÉS",
	"product_slide3_description_paragraph1":"¡Únete a Violetta en sus divertidas aventuras recorriendo 25 países diferentes y aprende palabras en inglés!",
	"product_slide3_description_paragraph2":"Basado en un método comprobado que combina enseñanza con historias, Violetta - inglés está diseñado para que los niños y adolescentes aprendan y practiquen el idioma de una manera fácil e interactiva.",
	"product_slide3_description_paragraph3":"¡Únete a Violetta y sus amigos y aprende inglés jugando!",
	"product_slide4_title1":"Kantoo inglés",
	"product_slide4_text":"¡Aprende inglés en una <br>Nueva York virtual!",
	"product_slide4_title2":"KANTOO INGLÉS",
	"product_slide4_description_paragraph1":"¿Siempre has soñado en hablar inglés? ¡Con Kantoo inglés tu sueño se hará realidad! El producto ofrece una manera eficaz y cautivante de aprender el idioma más deseado del planeta.",
	"product_slide4_description_paragraph2":"Únete a cuatro jóvenes provenientes de diferentes partes del mundo, que viven en la ciudad de Nueva York, aprenden inglés y persiguen sus sueños profesionales y personales en una de las ciudades más dinámicas del mundo.",
	"product_slide4_description_paragraph3":"¡Kantoo inglés es un producto audiovisual presentado por un profesor que habla castellano! Y además, el producto ofrece una historia cautivante e increíbles ejercicios interactivos.",
	"product_slide4_description_paragraph4":"Un curso completo en 3 niveles, 45 lecciones progresivas, donde se presentarán importantes puntos gramaticales, ejercicios de lectura, escritura, audición y pronunciación. ¡Todo eso mientras aprovechas una experiencia internacional!",
	"product_slide4_description_paragraph5":"¡Mejora tu inglés con Kantoo inglés!",
	"product_slide5_title1":"Kantoo francés",
	"product_slide5_text":"¡Aprende francés en <br>la ciudad de las luces!",
	"product_slide5_title2":"KANTOO FRANCÉS",
	"product_slide5_description_paragraph1":"El francés es el idioma del amor, del arte y Kantoo francés ofrece una excelente manera de aprender el idioma mientras vives la magia de París - la ciudad más romántica del mundo.",
	"product_slide5_description_paragraph2":"Acompaña la vida de cuatro estudiantes provenientes de diferentes partes del mundo, que buscan realizar sus sueños y aprender francés.",
	"product_slide5_description_paragraph3":"Tu profesora particular te acompañará a lo largo de todo el curso y los ejercicios interactivos que enfatizan los principales puntos gramaticales y la práctica de la pronunciación correcta harán que domines ese hermoso idioma.",	
	"product_slide5_description_paragraph4":"¡Mejora tu francés con Kantoo francés!",
	"product_slide6_title1":"Kantoo italiano",
	"product_slide6_text":"¡Todos los caminos <br>conducen a Roma!",
	"product_slide6_title2":"KANTOO ITALIANO",
	"product_slide6_description_paragraph1":"El italiano es un idioma musical, suena casi como una canción, ¡y ahora con Kantoo italiano aprenderás el idioma y al final del curso podrás incluso cantar en italiano!",
	"product_slide6_description_paragraph2":"Roma, la ciudad que combina historia y modernidad, recibe tres estudiantes provenientes de diferentes partes del mundo que vinieron a vivir experiencias increíbles y aprender italiano.",
	"product_slide6_description_paragraph3":"Cada lección presenta un vídeo con una profesora que habla castellano, una historia atractiva y varios ejercicios interactivos.",
	"product_slide6_description_paragraph4":"¡Mejora tu italiano con Kantoo italiano!",
	"product_slide7_title1":"Kantoo español",
	"product_slide7_text":"¡Una aventura <br>en Madrid!",
	"product_slide7_title2":"KANTOO ESPAÑOL",
	"product_slide7_description_paragraph1":"El español es uno de los idiomas más hablados en el mundo, ¡y con Kantoo español tendrás aún más posibilidades de comunicarte!",
	"product_slide7_description_paragraph2":"Únete a nosotros en un viaje fascinante a Madrid - una de las principales capitales culturales de Europa, y acompaña la vida de cuatro estudiantes provenientes de diferentes partes del mundoת que aprenden español y disfrutan del estilo de vida madrileño.",
	"product_slide7_description_paragraph3":"Una profesora particular te guiará a cerca de los principales puntos gramaticales y al mismo tiempo disfrutarás de todo lo que Madrid puede ofrecer - ¡la música, la comida, la emoción y el encanto de España!",	
	"product_slide7_description_paragraph4":"¡Mejora tu español con Kantoo español!",
	"product_slide8_title1":"Disney English Movie Trivia",
	"product_slide8_text":"¡Aprende inglés con<br>DISNEY!",
	"product_slide8_title2":"DISNEY ENGLISH MOVIE TRIVIA",
	"product_slide8_description_paragraph1":"Un servicio de web móvil en donde los niños están invitados a aprender inglés a través de preguntas acerca de sus películas favoritas de Disney, proporcionándoles una sensación de autonomía y una excelente manera de adquirir conocimiento.",
	"product_slide8_description_paragraph2":"Este servicio está disponible para usuarios de smartphones y otros modelos de teléfono con acceso a internet.",	
	"product_slide8_description_paragraph3":"¡Aprende inglés con tus personajes favoritos de Disney!",
	"title_careers":"TRABAJA CON NOSOTROS",
	"careers_text":"¡Únete a nuestro equipo!",
	"title_contact":"CONTÁCTANOS",
	"contact_text":"Please fill out the form",
	"contact_fullname":"Nombre y apellido",
	"contact_company":"Nombre de la compañía (opcional)",
	"contact_email":"E-mail",	
	"contact_message":"Mensaje",
	"contact_button":"Enviar",
	"contact_social":"O contáctanos vía:",
	"phone":"Número de teléfono",
	"profile":"Link de tu perfil en Linkedin",
	"submitApp":"Postúlate",	
	"submit_app_title":"Completa el formulario y postúlate",
	"resume":"Currículum",
	"portfolio":"Portafolio",
	"enter_resume":"Por favor, adjunta tu currículum.",
	"attach_file":"Seleccionar archivo",	
	"share":"Comparte esta oportunidad",
	"enter_name_contact":"Por favor, ingresa tu nombre y apellido.",
	"name_length_contact":"Por favor, no sobrepasar el límite de 32 caracteres.",
	"enter_email_contact":"Por favor, ingresa tu dirección de correo electrónico.",
	"invalid_email_contact":"Por favor, ingresa una dirección de correo electrónico válida.",
	"email_length_contact":"Por favor, no sobrepasar el límite de 32 caracteres.",
	"insert_message":"Por favor, escribe tu mensaje.",
	"message_length":"Por favor, no sobrepasar el límite de 240 caracteres.",
	"company_length":"Por favor, no sobrepasar el límite de 32 caracteres.",
	"captcha_error":"Error captcha",
	"phone_error":"Por favor, ingresa tu número de teléfono.",
	"404error":"Ups, hubo un error.",
	"report":"Reportar problema",
	"goback":"Página principal",
	"404text":"La página que has intentado acceder ya no está disponible.",	

	"previous-product": "<i class='fa fa-angle-left' aria-hidden='true'></i> Producto anterior",
	"next-product": "Producto siguiente <i class='fa fa-angle-right' aria-hidden='true'></i>",

	"privacy_title": "SITIO WEB DE KANTOO – POLÍTICA DE PRIVACIDAD",
	"first_policy": "Esta Política de Privacidad se aplica al Sitio web de Kantoo <a href='http://www.kantoo.com'><b>“www.kantoo.com”</b></a> (el &quot;<b>Sitio web</b>&quot;) y rige la recopilación y el uso de datos en el mismo y en relación con el mismo. Al visitar el Sitio web y acceder, navegar, usar cualquier contenido o al enviar información a él, usted reconoce y acepta las prácticas descritas en esta Política de Privacidad y con la recopilación y uso de información por parte de Kantoo de acuerdo con los términos establecidos a continuación.",
	"privacy_subtitle1": "Recolección de información y datos personales",
	"second_policy": "Kantoo puede recopilar, recibir y almacenar cualquier información, datos y otros detalles enviados, cargados, publicados, transmitidos, comunicados, compartidos o intercambiados por usted a través de sus visitas al Sitio web, otros sitios web de propiedad y/u operados por Kantoo u otros sitios web de terceros, accesibles u operados a través del Sitio web (colectivamente llamados de &quot;<b>Información Personal</b>&quot;). La Información Personal puede incluir, sin limitación:",
	"second_policy_li1": "Información personal que usted proporcione al completar y enviar formularios a Kantoo a través del Sitio web, como solicitudes de empleo a través de &quot;Trabaja con nosotros&quot; y/o formularios de consultas generales a través de &quot;Contáctanos&quot;. Esta información puede incluir, pero sin limitaciones, su nombre, dirección de correo electrónico, número de teléfono y otra información personal y/o de identificación u otros datos confidenciales;",
	"second_policy_li2": "Cierto tipo de información que Kantoo recibe, recopila y registra automáticamente al navegar por el Sitio web, incluyendo ciertos &quot;datos de tráfico&quot; de nuestros registros de servidor de su navegador, como su dirección IP, tipo de navegador, nombres de dominio (de Internet), horarios de acceso y direcciones de Sitios web de referencia. También podemos añadir en cualquier página el conteo total de visualizaciones;",
	"second_policy_li3": "Información demográfica anónima, que no es suya exclusivamente, como su código postal, edad, género, preferencias, intereses y favoritos;",
	"second_policy_li4": "Patrones de tráfico y uso del sitio que Kantoo puede utilizar para mejorar sus servicios.",
	"privacy_subtitle2": "Uso de la Información Personal",
	"third_policy": "A continuación enumeramos cómo Kantoo puede monitorear, recopilar, retener y utilizar cualquier Información Personal y textos de cualquier tipo que sean cargados o transmitidos por usted a través del Sitio web, incluyendo informaciones relacionadas a cualquier curriculum vitae cargado electrónicamente por usted a Kantoo, si y como aplicable, a través del enlace &quot;Trabaja con nosotros&quot; en el Sitio web:",
	"third_policy_li1": "Usando los datos de tráfico recopilados por Kantoo por razones de control de calidad para ayudar a diagnosticar problemas con sus servidores.",
	"third_policy_li2": "Produciendo estadísticas e información estadística sobre el uso del Sitio web y transmitiendo dicha información a terceros de acuerdo con el criterio de Kantoo. No se proporcionarán detalles de identificación a terceros, sujeto a las excepciones especificadas aquí.",
	"third_policy_li3": "Procesando sus consultas y respondiendo a sus solicitudes.",
	"third_policy_li4": "Kantoo puede utilizar su información para proporcionar y personalizar el Sitio web y para detectar y/o prevenir fraudes. Kantoo también puede utilizar dicha información para fines de promoción, como ofrecer a los usuarios determinados productos o servicios de Kantoo o de otros. Kantoo también puede contactar a los usuarios a través de encuestas para examinar los servicios actuales o potenciales y/o funcionalidades del Sitio web.",
	"third_policy_li5": "Kantoo puede monitorear y acompañar el Sitio web en general, así como las visitas de los usuarios al Sitio web, para determinar la popularidad de ciertos recursos, funcionalidades y otros fines, incluyendo fines comerciales. Kantoo puede utilizar estos datos para mejorar, personalizar sus servicios online y entregar contenido personalizado y publicidad a los usuarios cuyo comportamiento indica que están interesados en un área específica.",
	"third_policy_li6": "Kantoo puede, de vez en cuando, ponerse en contacto con usted, ya sea por cuenta propia o en nombre de terceros, acerca de una oferta específica que puede ser de su interés y/o para enviar sus actualizaciones e información de marketing (que también puede incluir materiales de propiedad de terceros) y/o con fines comerciales adicionales, como la promoción de ventas y comercialización de productos y servicios de Kantoo. Para estos fines, Kantoo puede ponerse en contacto con los usuarios utilizando información de identificación personal (correo electrónico, nombre, dirección, número de teléfono), pero dicha información no se transferirá a terceros. Usted autoriza específicamente a Kantoo a utilizar su nombre e información de contacto para actividades de marketing, promocionales y comerciales en general.",
	"third_policy_li7": "Además, Kantoo puede compartir información de identificación personal con terceros que sean proveedores de servicios de Kantoo, vinculados por compromisos de confidencialidad y comprometidos a ayudar a realizar análisis estadísticos, enviar correos electrónicos, correo postal o proporcionar soporte al cliente. Todos los terceros están prohibidos de utilizar su información personal, excepto para proporcionar estos servicios a Kantoo, con excepción de proporcionar estos servicios a Kantoo, y son o serán (según sea el caso) necesarios para mantener la confidencialidad de su información.",
	"third_policy_li8": "Kantoo comparte Información Personal con sus socios comerciales estratégicos para suministrar productos y servicios y/o para ayudar a comercializar dichos productos y servicios a usuarios finales, así como con empresas que prestan servicios a Kantoo, como alojamiento web, procesamiento de datos, recopilación de tarifas de suscripción, administración y potencializar los datos de clientes, soporte al cliente, evaluación de interés en nuestros productos y servicios y conducir la análisis de clientes o las encuestas de satisfacción.",
	"third_policy_li9": "Usted tiene el derecho de solicitar por escrito que su información sea eliminada de todas y cada una de las bases de datos de Kantoo. Sin embargo, usted reconoce que la eliminación de su Información Personal puede resultar en su incapacidad de usar todos o algunos de los servicios del Sitio web y usted no podrá tener ninguna queja contra Kantoo con respecto a su incapacidad de usar dichos servicios debido a lo expuesto anteriormente.",
	"third_policy_li10": "La información que usted proporciona puede ser transferida o accedida por instituciones de todo el mundo según se describe en esta Política de Privacidad. Al usar nuestro Sitio web, y al enviar cualquier Información Personal, representa su aceptación con dicha transferencia.",
	"fourth_policy": "Sujeto a las excepciones especificadas en este documento, la Información Personal recopilada por Kantoo no se revelará a ningún tercero sin que usted previamente autorice por escrito. Kantoo sólo divulgará la Información Personal a terceros, tal como se provee a través del Sitio web, si: (a) Kantoo está obligado a hacerlo por ley, orden judicial, medida cautelar o decreto; (b) Kantoo cree de buena fe que tal acción es necesaria para adecuarse o cumplir con las disposiciones de cualquier ley o procedimiento judicial, para proteger los derechos o bienes de Kantoo y/o para proteger la seguridad personal de los usuarios del Sitio web o de su público; (c) Hay disputa entre Kantoo y un usuario del Sitio web; (d) Es necesario verificar su identidad y prevenir o detectar fraudes; (5) Existe el consentimiento del usuario. Tenga en cuenta que cualquier transferencia de su Información Personal puede considerarse una transferencia internacional de datos.",
	"privacy_subtitle3": "Privacidad de los niños",
	"fifth_policy": "Proteger la privacidad de los niños es muy importante. Kantoo no recopila o solicita conscientemente información personal a nadie menor de 13 años ni permite que tales personas utilicen el Sitio web. Si usted tiene menos de 13 años, no envíe ninguna información personal a través del Sitio web. Alentamos a los padres y responsables legales a monitorear a sus hijos en el uso de Internet, comportamiento online, actividades e intereses, además de ayudarnos a cumplir con nuestra Política de Privacidad, instruyendo a sus hijos a nunca más proporcionar información personal en nuestro Sitio web. ",
	"privacy_subtitle4": "Otros sitios web ",
	"sixth_policy": "El Sitio web de Kantoo puede contener enlaces a sitios pertenecientes u operados por Kantoo y servicios relacionados ofrecidos por Kantoo, que se rigen específicamente por sus propios términos de uso y política de privacidad. Sin perjuicio de lo anterior, el Sitio web de Kantoo puede contener enlaces a sitios web de terceros que están fuera del control de Kantoo y no están cubiertos por esta Política de Privacidad. Si accede a otros sitios web utilizando los enlaces provistos en el Sitio web de Kantoo, los responsables de estos sitios pueden recopilar información que ellos podrán utilizar de acuerdo a su propia política de privacidad que puede diferir de esta. ",
	"privacy_subtitle5": "Servicios de terceros",
	"seventh_policy": "Kantoo puede usar Google Analytics, Amazon y/u otros servicios de terceros similares (colectivamente llamados de &quot;<b>Servicios de Terceros</b>&quot;) para ayudarnos a mantener y comprender el uso de nuestro Sitio web. Dichos Servicios de Terceros recopilan la información enviada por su navegador como parte de una solicitud de página web, incluyendo cookies y su dirección IP. Los Servicios de Terceros también reciben esta información y su uso se rige por sus respectivas políticas de privacidad. Para saber cómo Google Analytics recopila y procesa datos, utilice el siguiente enlace: <a href=&quot;www.google.com/policies/privacy/partners/&quot;>“Cómo utiliza Google los datos cuando usted accede a los sitios o utiliza las aplicaciones de nuestros socios”</a>, que se encuentra en www.google.com/policies/privacy/partners/ (o en cualquier otro URL que Google puede proporcionar de vez en cuando).",
	"privacy_subtitle6": "Uso de cookies",
	"eigth_policy": "El Sitio web utiliza &quot;cookies&quot; (ya sea de Kantoo o de terceros) para ayudarnos a personalizar su experiencia online. Una cookie es un archivo de texto que un servidor de página web coloca en su disco duro. <br> Uno de los principales propósitos de las cookies es proporcionar la función de ahorro de tiempo. La finalidad de una cookie es decirle al servidor web que usted ha regresado a una página específica. Por ejemplo, si usted visita nuestro Sitio web, una cookie nos ayuda a recordar su información específica en visitas posteriores, esto simplifica el proceso de registro de su información personal y cuando regrese al Sitio web, la información proporcionada anteriormente puede ser recuperada para que usted puede utilizar fácilmente los recursos que ha personalizado.<br>Usted puede aceptar o rechazar las cookies. La mayoría de los navegadores web aceptan automáticamente las cookies, pero si lo prefiere, puede modificar la configuración de su navegador para rechazarlas. Si elige rechazar las cookies, es posible que no pueda explorar completamente todas las características interactivas de nuestro Sitio web.",
	"privacy_subtitle7": "Seguridad de su Información Privada",
	"ninth_policy": "Kantoo asegura de forma razonable la información personalmente identificable que usted proporciona a través del acceso no autorizado, uso o divulgación en servidores de ambiente controlado y asegurado, protegido de acceso no autorizado. Sin embargo, usted reconoce que el Sitio web y sus sistemas de protección de información no son inmunes a ninguna violación o sabotaje, y terceros pueden hacer uso ilícito y dañino del Sitio web, incluyendo, pero sin limitarse, la carga de archivos maliciosos de cualquier tipo, virus, worms, caballo de Troya, entre otros. Usted acepta que Kantoo no será responsable, de ningún modo, por cualquier daño que pueda resultar de cualquier comportamiento de terceros.",
	"privacy_subtitle8": "Cambios en esta Política de Privacidad",
	"tenth_policy": "Kantoo puede actualizar esta Política de Privacidad de vez en cuando, sin previo aviso, reemplazándola en el Sitio web por una Política de Privacidad actualizada. La Política de Privacidad publicada en un dado momento reemplazará automáticamente la anterior Política de Privacidad y ésta debe regir las relaciones entre Kantoo y los usuarios.",
	"privacy_subtitle9": "Limitación de Responsabilidad",
	"eleventh_policy": "EN NINGÚN CASO KANTOO, SUS SUBSIDIARIOS, SUS OFICIALES, DIRECTORES, ACCIONISTAS, EMPLEADOS, CONSULTORES, AGENTES, REPRESENTANTES, SOCIOS COMERCIALES, AFILIADOS Y CUALQUIER OTRO EN SU NOMBRE (&quot;SUSCRIPTOS DE KANTOO&quot;), SERÁN RESPONSABLES DE CUALQUIER DAÑO O PÉRDIDA RESULTANTE DEL USO DE INFORMACIÓN PERSONAL POR KANTOO O POR CUALQUIER UNO DE SUS SUSCRIPTOS O EL INCUMPLIMIENTO POR PARTE DE KANTOO O CUALQUIER UNO DE SUS SUSCRIPTOS, YA SEAN BASADOS EN GARANTÍA, CONTRATO, DELITO O CUALQUIER OTRA TEORÍA LEGAL, Y SEA O NO KANTOO ADVERTIDO DE LA POSIBILIDAD DE TALES DAÑOS. LA LIMITACIÓN DE RESPONSABILIDAD SE APLICARÁ EN LA MÁXIMA EXTENSIÓN PERMITIDA POR LA LEY EN LA JURISDICCIÓN APLICABLE. EN CASO DE QUE, A PESAR DE LO ANTERIOR, KANTOO O CUALQUIERA DE SUS SUSCRIPTOS SE ENCUENTREN RESPONSABLES POR DAÑOS DE CUALQUIER TIPO CON RELACIÓN A LA INFORMACIÓN PERSONAL Y/O A ESTA POLÍTICA DE PRIVACIDAD, EN NINGÚN CASO, TAL RESPONSABILIDAD EXCEDERÁ EL TOTAL DE U$ 1.000.",
	"privacy_subtitle10": "Contacto",
	"twelfth_policy": "En caso de cualquier duda o consulta, póngase en contacto con La Mark Vision Ltd. por el teléfono: +<b>972-9-9606666</b> o por correo electrónico: <b>info@la-mark.com</b> y utilizaremos esfuerzos comerciales razonables para atender su solicitud.",
	"privacy_subtitle11": "Observaciones",
	"thirtheenth_policy": "Esta Política de Privacidad se regirá exclusivamente por las leyes sustantivas internas del Estado de Israel, sin tener en cuenta los principios de conflicto de leyes. Cualquier reclamo o disputa entre Usuario y Kantoo que ocurra totalmente o parcialmente en conexión con esta Política de Privacidad será decidida exclusivamente por un tribunal de jurisdicción competente ubicado en Tel Aviv, Israel. ",
	"privacy_subtitle12": "Fecha de vigencia",
	"fourteenth_policy": "Esta Política de Privacidad se actualizó el 17 de diciembre de 2017.",
	"termsConditions" : "Política de Privacidad"
}