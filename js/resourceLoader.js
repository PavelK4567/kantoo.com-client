    var urlConstant = "o.com";
    //TODO
    var urlConstantLength = 5;

    function setLanguageInUrl(lang){
        var currentURL = window.location.href;
        var langUrl = currentURL.substring(currentURL.indexOf(urlConstant) + 6 , currentURL.indexOf(urlConstant) + 8);
        if (langUrl == undefined || langUrl.length == 0) {
            addLanguage(lang);
        }else{
            var currentUrlLanguage = currentURL.substring(currentURL.indexOf(urlConstant) + 5 , currentURL.indexOf(urlConstant) + 9);
            if(currentUrlLanguage === "/en/" || currentUrlLanguage === "/es/" || currentUrlLanguage === "/pt/"){
                replaceLanguage(lang);
            }else{
                addLanguage(lang);
            }
        }
    }

    function replaceLanguage(lang){
        var currentURL = window.location.href;
        var langInUrl = currentURL.substring(currentURL.indexOf(urlConstant) + 6 , currentURL.indexOf(urlConstant) + 8);
                history.pushState(null, null, currentURL.replace(urlConstant + "/" + langInUrl, urlConstant + "/" + lang));
    }

    function addLanguage(lang){
        var currentURL = window.location.href;
        history.pushState(null, null, currentURL.replace(urlConstant, urlConstant + "/" +lang));
    }

    function getLanguageFromUrl(){
        var currentURL = window.location.href;
        var lang = currentURL.substring(currentURL.indexOf(urlConstant) + 6 , currentURL.indexOf(urlConstant) + 8);

        if (lang == undefined || lang.length == 0 ) {
          setLanguageInUrl("en");
          return "en";
        }else{
            if(lang != "en" || lang != "es" || lang != "pt")
            return lang;
        }
    }
	function loadEnglish(){
        $('#lang-en').addClass("active");
        $('#lang-es').removeClass("active");
        $('#lang-pt').removeClass("active");
		loadResources("en");
        $('#activeLang').text("EN");
        $(".pictureEN").css("display", "inline-block");
        $(".pictureES").css("display", "none");
        $(".en-banners").css("display", "inline-block");
        $(".pt-banners").css("display", "none");
        $(".es-banners").css("display", "none");
        $("video").each(function () { this.pause() });
      /*  $(".jobs-EN").css("display", "block");
        $(".jobs-ES").css("display", "none");
        $(".jobs-PT").css("display", "none");*/
	}
	function loadSpanish(){
        $('#lang-es').addClass("active");
        $('#lang-en').removeClass("active");
        $('#lang-pt').removeClass("active");
		loadResources("es");
        $('#activeLang').text("ES");
        $(".pictureEN").css("display", "none");
        $(".pictureES").css("display", "inline-block");
        $(".es-banners").css("display", "inline-block");
        $(".pt-banners").css("display", "none");
        $(".en-banners").css("display", "none");
        $("video").each(function () { this.pause() });
        /*$(".jobs-EN").css("display", "none");
        $(".jobs-ES").css("display", "block");
        $(".jobs-PT").css("display", "none");*/
	}
	function loadPortuguese(){
        $('#lang-pt').addClass("active");
        $('#lang-es').removeClass("active");
        $('#lang-en').removeClass("active");
		loadResources("pt");
        $('#activeLang').text("PT");
        $(".pictureEN").css("display", "inline-block");
        $(".pictureES").css("display", "none");
        $(".pt-banners").css("display", "inline-block");
        $(".en-banners").css("display", "none");
        $(".es-banners").css("display", "none");
        $("video").each(function () { this.pause() });
       /* $(".jobs-EN").css("display", "none");
        $(".jobs-ES").css("display", "none");
        $(".jobs-PT").css("display", "block");*/
	}

// Try to get language info from the browser.
function loadResources(lang){
    //find language
    if (lang && lang.length > 0) {
        if (lang != 'en' && lang != 'es' && lang != 'pt') {
            lang = 'en';
        }
    }
    setLanguageInUrl(lang);
    //add css
    if(lang == "en"){
        $('#lang-en').addClass("active");
        $(".pictureEN").css("display", "block");
        $(".pictureES").css("display", "none");
        $(".en-banners").css("display", "inline-block");
        $(".pt-banners").css("display", "none");
        $(".es-banners").css("display", "none");
    }
    if(lang == "es"){
        $('#lang-es').addClass("active");
        $(".pictureEN").css("display", "none");
        $(".pictureES").css("display", "block");
        $(".es-banners").css("display", "inline-block");
        $(".pt-banners").css("display", "none");
        $(".en-banners").css("display", "none");
    }
    if(lang == "pt"){
        $('#lang-pt').addClass("active");
        $(".pictureEN").css("display", "block");
        $(".pictureES").css("display", "none");
        $(".pt-banners").css("display", "inline-block");
        $(".en-banners").css("display", "none");
        $(".es-banners").css("display", "none");
    }

// Construct a language-specific resource path.
var resourcePath = 'js/lang_' + lang + '.js';
// Get a reference to the HEAD element of the HTML page.
var head = document.head || document.getElementsByTagName('head')[0];
// Dynamically add the resourcePath to the HEAD element
// to start loading the resources.
var scriptEl = document.createElement('script');
scriptEl.type = 'text/javascript';
scriptEl.src = resourcePath;
head.appendChild(scriptEl);
scriptEl.onload = function () {
    updateResourcesValues()
};

}
function updateResourcesValues()
{

var resElms = document.querySelectorAll('[data-res]');
for (var n = 0; n < resElms.length; n++) {
    var resEl = resElms[n];
    // Get the resource key from the element.
    var resKey = resEl.getAttribute('data-res');
    if (resKey) {
        // Get all the resources that start with the key.
        for (var key in resources) {
            if (key.indexOf(resKey) == 0) {
                var resValue = resources[key];
                if (key.indexOf('.') == -1) {
                    // No dot notation in resource key,
                    // assign the resource value to the element's
                    // innerHTML.
                    resEl.innerHTML = resValue;
                }
                else {
                    // Dot notation in resource key, assign the
                    // resource value to the element's property
                    // whose name corresponds to the substring
                    // after the dot.
                    var attrKey = key.substring(key.indexOf('.') + 1);
                    resEl[attrKey] = resValue;
                }
            }
        }
    }
}
}
